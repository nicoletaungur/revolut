﻿using Revolut.DtoModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Revolut.ModelsDTO.Group
{
    public class GroupDTO
    {
        public int Id { get; set; }
        [Required]
        [MinLength(3), MaxLength(30)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }

        [Required]
        public UserDTO Owner { get; set; }
        public IList<UserDTO> Members { get; set; }
    }
}
