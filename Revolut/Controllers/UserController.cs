﻿using Microsoft.AspNetCore.Mvc;
using Revolut.DtoModels;
using Revolut.Services;
using System;

namespace Revolut.Controllers
{
	[Route("api/users")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private IUserService _userService;

		public UserController(IUserService userService)
		{
			_userService = userService;
		}

		[HttpGet()]
		public IActionResult GetUsers()
		{
			var users = _userService.GetUsers();

			return Ok(users);
		}

		[HttpGet("{cnp}")]
		public IActionResult GetUser(string cnp)
		{
			var user = _userService.GetUser(cnp);

			if (user == null)
				return NotFound($"The user with id = {cnp} does not exist ");

			return Ok(user);
		}

		[HttpGet("full/{cnp}")]
		public IActionResult GetFullUser(string cnp)
		{
			var user = _userService.GetFullUser(cnp);

			if (user == null)
				return NotFound($"The user with id = {cnp} does not exist ");

			return Ok(user);
		}

		[HttpGet("search")]
		public IActionResult GetUserByName([FromQuery]string firstName, [FromQuery]string lastName)
		{
			var users = _userService.GetUserByName(firstName, lastName);

			if (users.Count == 0)
				return NotFound($"The users are not found ");

			return Ok(users);
		}

		[HttpPost()]
		public IActionResult AddUser([FromBody] CreateUserDTO user)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			var u = _userService.AddUser(user);


			if (u == null)
				return NotFound("User with this CNP already exist");

			return Ok(u);
		}

		[HttpPut()]
		public IActionResult UpdateUser([FromBody] CreateUserDTO user)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			var u = _userService.UpdateUser(user);

			if (u == null)
				return NotFound($"Doesn't exist a user with this id = {user.CNP}");

			return Ok(u);
		}

		[HttpDelete("{cnp}")]
		public IActionResult DeleteUser(string cnp)
		{
			try
			{
				var user = _userService.DeleteUser(cnp);
				return Ok(user);
			}
			catch (NullReferenceException ex)
			{
				return BadRequest(ex.Message);
			}
			catch (Exception ex)
			{
				return BadRequest(ex);
			}
		}
	}
}