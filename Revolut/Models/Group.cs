﻿using System.Collections.Generic;

namespace Revolut.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public User Owner { get; set; }
        public IList<User> Members { get; set; }
    }
}
