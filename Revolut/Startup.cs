﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Revolut.Interfaces.Mapping;
using Revolut.Interfaces.Repositories;
using Revolut.Interfaces.Services;
using Revolut.Mapping;
using Revolut.Repositories;
using Revolut.Services;

namespace Revolut
{
	public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}
		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc();
			services.AddAutoMapper();

			services.AddSingleton(Configuration);

			// Services

			services.AddScoped<IUserService, UsersService>();
			services.AddScoped<IGroupService, GroupService>();
			services.AddScoped<IBankAccountService, BankAccountService>();

			// Mappings

			services.AddScoped<IUserMapping, UserMapping>();
			services.AddScoped<IGroupMapping, GroupMapping>();
			services.AddScoped<IBankAccountMapping, BankAccountMapping>();

			// Repository

			services.AddScoped<IUserRepository, UserRepository>();


		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseMvc();
			app.UseStatusCodePages();
		}
	}
}
