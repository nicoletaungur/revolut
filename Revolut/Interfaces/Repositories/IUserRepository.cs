﻿
using System.Collections.Generic;
using Revolut.Models;

namespace Revolut.Interfaces.Repositories
{
	public interface IUserRepository
	{
		IList<User> GetAll();
		User GetByCnp(string cnp);
		User GetFullUserByCnp(string cnp);
		User Add(User user);
		void Update(User user);
		void Delete(string cnp);
	}
}
