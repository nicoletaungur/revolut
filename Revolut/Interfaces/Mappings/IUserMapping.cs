﻿using Revolut.DtoModels;
using Revolut.Models;

namespace Revolut.Mapping
{
    public interface IUserMapping
    {
        UserDTO GetUserDTO(User user);
        User GetModelFromUserDTO(UserDTO userDTO);

        CreateUserDTO GetCreateUserDTO(User user);
        User GetModelFromCreateUserDTO(CreateUserDTO createUserDTO);
    }
}