﻿using Revolut.Interfaces.Repositories;
using Revolut.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Revolut.Repositories
{
	public class UserRepository : IUserRepository
	{
		private string _connectionString;
		private DbConnection _dbConnection;

		public UserRepository(IConfiguration configuration)
		{
			_connectionString = configuration["connectionStrings:revolutDB"];
			_dbConnection = new SqlConnection(_connectionString);
		}

		public User Add(User user)
		{
			var sql = "Insert Into Users(Cnp, FirstName, LastName, UserName, Password, Address) " +
			          "Values(@Cnp , @FirstName, @LastName, @UserName, @Password, @Address)";

			_dbConnection.Query(sql, user);

			return user;
		}

		public void Delete(string cnp)
		{
			var sql = "Delete From Users Where Cnp = @Cnp";
			_dbConnection.Query(sql, new {Cnp = cnp});
		}

		public IList<User> GetAll()
		{
			var sql = "SELECT * FROM Users";
			var users = _dbConnection.Query<User>(sql).ToList();
			return users;
		}

		public User GetFullUserByCnp(string cnp)
		{
			var sql = "Select * From Users where Cnp = @cnp " +
			          "Select* From BankAccounts where OwnerCnp = @cnp " +
			          "Select* From Groups Where Id in (Select Id From UsersGroups Where OwnerCnp = @cnp)";

			using (var multipleResuls = _dbConnection.QueryMultiple(sql, new {cnp = cnp}))
			{
				var user = multipleResuls.Read<User>().FirstOrDefault();
				var bankAccounts = multipleResuls.Read<BankAccount>().ToList();
				var groups = multipleResuls.Read<Group>().ToList();

				if (user != null)
				{
					user.BankAccounts = bankAccounts;
					user.Groups = groups;
				}

				return user;
			}
		}

		public void Update(User user)
		{
			var sql = "Update Users SET " +
			          "Cnp = @Cnp, " +
			          "FirstName = @FirstName, " +
			          "LastName = @LastName, " +
			          "UserName = @UserName, " +
			          "Password = @Password, " +
			          "Address = @Address " +
			          "Where Cnp = @Cnp"
				;

			_dbConnection.Query(sql, user);

		}

		public User GetByCnp(string cnp)
		{
			var sql = "SELECT * FROM Users WHERE cnp = @cnp";
			var user = _dbConnection.Query<User>(sql, new { cnp = cnp }).FirstOrDefault();
			return user;
		}
	}
}
